﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastShoot : MonoBehaviour
{
    public int laserDamage = 1;
    public float fireRate = 0.25f;
    public float weaponRange = 20f;
    public Transform laserSpawn;
    public AudioSource laserAudio;
    public Camera johnCam;

    private WaitForSeconds shotDuration = new WaitForSeconds(0.07f);
    private LineRenderer laserLine;
    private float nextFire;

    // Start is called before the first frame update
    void Start()
    {
        laserLine = GetComponent<LineRenderer>();
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject player = GameObject.Find("JohnLemon");
        PlayerMovement playerMovement = player.GetComponent<PlayerMovement>();
        //if John has the weapon
        if (playerMovement.allowShooting)
        {
            
            if (Input.GetKeyDown(KeyCode.Space))
            {
                nextFire = Time.time + fireRate;  //Only allow john to shoot a limited amount of times in given time frame

                StartCoroutine(ShotEffect());
                //Points the raycast to the center of the screen
                Vector3 rayOrigin = johnCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
                RaycastHit hit;

                laserLine.SetPosition(0, laserSpawn.position);
                //if the raycast hits an object
                if (Physics.Raycast(rayOrigin, johnCam.transform.forward, out hit, weaponRange))
                {
                    //stop the laser at the object
                    laserLine.SetPosition(1, hit.point);
                    //remove health from object
                    Shootable health = hit.collider.GetComponentInParent<Shootable>();

                    if (health != null)
                    {
                        //destroy the object if it has 0 health
                        health.Damage(laserDamage);
                        
                    }
                }
                else
                {
                    //if the raycast didnt hit an object, let it extend to its max range
                    laserLine.SetPosition(1, rayOrigin + (johnCam.transform.forward * weaponRange));
                }
            }
        }
    }

    //display or plays the laser animation
    private IEnumerator ShotEffect()
    {
        laserAudio.Play();

        laserLine.enabled = true;
        yield return shotDuration;
        laserLine.enabled = false;
    }

    
}
