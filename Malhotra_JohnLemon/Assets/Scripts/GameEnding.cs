﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public CanvasGroup killedBackgroundImageCanvasGroup;
    public AudioSource killedAudio;

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    bool m_IsPlayerDead;
    float m_Timer;
    bool m_HasAudioPlayed;
    float displayImageDuration = 1f;

    void OnTriggerEnter(Collider other)
    {
        //if the player triggered the event, set IsPlayerAtExit to true
        if(other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
        }
    }
    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }

    public void KilledPlayer()
    {
        m_IsPlayerDead = true;
        displayImageDuration += 1f;
    }

    void Update()
    {
        //if the player is at the exit, call the end level function and end the game
        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        //if the player is caught, call the end level function and restart the level
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
        else if (m_IsPlayerDead)
        {
            EndLevel(killedBackgroundImageCanvasGroup, true, killedAudio);
        }
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
        //start timer
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        //once the animation ends
        if (m_Timer > fadeDuration + displayImageDuration)
        {
            //if we are restarting/player was caught
            if (doRestart)
            {
                //Load up the first scene
                SceneManager.LoadScene(1);
            }
            //otherwise quit the game
            else
            {
                Application.Quit();
            }
        }
    }
}
