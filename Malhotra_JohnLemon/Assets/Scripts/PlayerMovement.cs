﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public float sprintSpeed = 5f;
    public bool allowShooting = false;
    public Transform particleEffect;
    public GameEnding gameEnding;

    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity; //Quaternions store rotations
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

        particleEffect.GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Setting up movement
        float horizontal = Input.GetAxis("Horizontal"); 
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);
        //When john is walking play the footstep audio, if hes not stop the audio
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }
        //let john sprint when leftshift is pressed
        if (Input.GetKey(KeyCode.LeftShift))
        {
            m_Movement *= sprintSpeed;
        }
        //Calculated character's forward vector
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, 
            turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
        
        
    }
    //Method allows root motion to be applied and move the player
    void OnAnimatorMove()
    {
        //Movement
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * 
            m_Animator.deltaPosition.magnitude);
        //Rotation
        m_Rigidbody.MoveRotation(m_Rotation);
    }
    private void OnTriggerEnter(Collider other)
    {
        //If john walks into the weapon, play the particle effect, give him shooting capabilities, and destroy the 
        if (other.gameObject.CompareTag("Weapon"))
        {
            particleEffect.GetComponent<ParticleSystem>().Play();
            allowShooting = true;
            Destroy(other.gameObject);
            
        }
        //if john walks into the trap, call KilledPlayer method
        else if (other.gameObject.CompareTag("Trap"))
        {
            gameEnding.KilledPlayer();
        }
    }
}
