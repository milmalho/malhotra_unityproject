﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFight : MonoBehaviour
{
    public GameObject player;
    public PanCam other;
    public GameObject wallToDestroy;
    public DialogController dc;

    private bool panCamEnabled = false;

    private void OnTriggerEnter(Collider other)
    {
        //once john enters the boss' room
        if(other.gameObject == player)
        {
            //switch to pancam and display the text that the boss is saying and disable the trigger
            StartCoroutine(Boss());
            dc.DisplayText();
            GetComponent<Collider>().enabled = false;
        }
    }

    //switches from pancam to maincam and vice versa
    IEnumerator Boss()
    {
        other.ShowPanCam();
        panCamEnabled = true;
        yield return new WaitForSeconds(8);

        other.ShowPlayerCam();
        panCamEnabled = false;
        Destroy(wallToDestroy);
        other.ShowBossCam();
    }
}
