﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogController : MonoBehaviour
{
    public float delay = 0.1f;
    public string fullText;
    private string currentText = "";

    
    // Start is called before the first frame update
    public void DisplayText()
    {
        //start the showtext coroutine
        StartCoroutine(ShowText());
    }
    
    //display the boss' text
    private IEnumerator ShowText()
    {
        //display text until there's no more text to display
        for(int i = 0; i < fullText.Length; i++)
        {
            currentText = fullText.Substring(0, i);
            this.GetComponent<TextMeshProUGUI>().text = currentText;
            yield return new WaitForSeconds(delay);
        }
         
    }
}
