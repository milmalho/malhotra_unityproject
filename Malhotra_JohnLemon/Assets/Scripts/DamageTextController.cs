﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTextController : MonoBehaviour
{
    private static DamageText damageText;
    private static GameObject canvas;

    public static void Initialize()
    {
        canvas = GameObject.Find("Canvas");
        if (!damageText)
        {
            damageText = Resources.Load<DamageText>("Prefabs/DamageTextParent");
        }
    }
    public static void CreateDamageText(string text, Transform location)
    {
        DamageText instance = Instantiate(damageText);
        instance.transform.SetParent(canvas.transform, false);
        instance.SetText(text);
    }
}
