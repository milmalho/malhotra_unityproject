﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//Code commented out was for displaying score. Could not get the functionality to work properly
public class Shootable : MonoBehaviour
{
    public int currentHealth = 1;

    private int hit = 50;
    private int kill = 100;
    

    private void Start()
    {
        DamageTextController.Initialize();
    }
   

    public void Damage(int damageAmount)
    {
        
        //take away some health when hit (this method is called in RayCastShoot)
        currentHealth -= damageAmount;
        if (currentHealth > 0)
        {
            DamageTextController.CreateDamageText("+" + hit.ToString(), transform);
        }

        else if (currentHealth == 0)
        {
            DamageTextController.CreateDamageText("+" + kill.ToString(), transform);
            gameObject.SetActive(false);
            
        }
    }
}
